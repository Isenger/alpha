import fizzbuzz
import unittest


class TestFizzBuzz(unittest.TestCase):
    #ค่า n > 10000
    def test_give_over1_shoud_be_not_found(self):
        self.assertEqual(fizzbuzz.fizzbuzz(12054), 'NoFizzBuzz', 'Should be NoFizzBuzz')

    def test_give_over2_shoud_be_not_found(self):
        self.assertEqual(fizzbuzz.fizzbuzz(127377), 'NoFizzBuzz', 'Should be NoFizzBuzz')

    #ค่า n < 0
    def test_give_minus1_shoud_be_not_found(self):
        self.assertEqual(fizzbuzz.fizzbuzz(-125), 'NoFizzBuzz', 'Should be NoFizzBuzz')

    def test_give_minus2_shoud_be_not_found(self):
        self.assertEqual(fizzbuzz.fizzbuzz(-2460), 'NoFizzBuzz', 'Should be NoFizzBuzz')

    #เช็คเงื่อนไขในกรณีที่คค่า n > 0 && n !> 10000
    def test_give_3_shoud_be_fizz(self):
        self.assertEqual(fizzbuzz.fizzbuzz(3), 'Fizz', 'Should be Fizz')
 
    def test_give_5_shoud_be_buzz(self):
        self.assertEqual(fizzbuzz.fizzbuzz(5), 'Buzz', 'Should be Buzz')

    def test_give_15_shoud_be_fizzbuzz(self):
        self.assertEqual(fizzbuzz.fizzbuzz(15), 'FizzBuzz', 'Should be FizzBuzz')
    
    def test_give_9_shoud_be_fizz(self):
        self.assertEqual(fizzbuzz.fizzbuzz(9), 'FizzFizz', 'Should be FizzFizz')
 
    def test_give_25_shoud_be_buzz(self):
        self.assertEqual(fizzbuzz.fizzbuzz(25), 'BuzzBuzz', 'Should be BuzzBuzz')

    def test_give_225_shoud_be_fizzbuzz(self):
        self.assertEqual(fizzbuzz.fizzbuzz(225), 'FizzFizzBuzzBuzz', 'Should be FizzFizzBuzzBuzz')

    
if __name__ == '__main__':
    unittest.main()
