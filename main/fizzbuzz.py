def fizzbuzz(n):
    if(n > 10000 | n < 0):
        return "NoFizzBuzz"
    else:
        if(n % 9 == 0) & (n % 25 == 0):
            return"FizzFizzBuzzBuzz"
        elif(n % 9 == 0):
            return"FizzFizz"
        elif(n % 25 == 0):
            return"BuzzBuzz"
        elif(n % 3 == 0) & (n % 5 == 0):
            return"FizzBuzz"
        elif(n % 3 == 0):
            return"Fizz"
        elif(n % 5 == 0):
            return"Buzz"
